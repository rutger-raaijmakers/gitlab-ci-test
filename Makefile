export PYTHONPATH := $PYTHONPATH:$(pwd)

train:
	@echo 'Training model'
	@echo
	python machine_learning/$(model)/controller/object.py
	@echo

format:
	@echo 'Formatting code'
	@echo
	black -l 80 .
	@echo