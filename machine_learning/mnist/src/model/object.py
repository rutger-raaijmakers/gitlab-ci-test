import os
import torch
import torch.nn.functional as F
import torch.optim as optim
from pathlib import Path
from lib.common.ml.net import Net
from torch.optim.lr_scheduler import StepLR
from torchvision import datasets, transforms

BATCH_SIZE = 1
PATH_MODEL = Path(os.path.dirname(__file__)) / ".." / ".." / "model"
PATH_DATA = Path(os.path.dirname(__file__)) / ".." / ".." / "data"


class ModelMNIST:
    def __init__(self, args):
        self.args = args
        self.device = self._determine_device()
        self.model = self._determine_model()
        self.optimizer = self._determine_optimizer()
        self.scheduler = self._determine_scheduler()
        self.train_loader = self._determine_train_loader()
        self.test_loader = self._determine_test_loader()
        self._set_torch_seed()
        self.batch_size = BATCH_SIZE

    def train(self):
        for epoch in range(1, self.args.epochs + 1):
            self._epoch_train(epoch)
            self._ecoch_test()
            self.scheduler.step()

    def export_to_onnx(self):
        dummy_input = torch.randn(
            self.batch_size, 1, 28, 28, device=self.device
        )
        torch.onnx.export(self.model, dummy_input, PATH_MODEL / "mnist.onnx")

    def _epoch_train(self, epoch):
        self.model.train()
        for batch_idx, (data, target) in enumerate(self.train_loader):
            data, target = data.to(self.device), target.to(self.device)
            self.optimizer.zero_grad()
            output = self.model(data)
            loss = F.nll_loss(output, target)
            loss.backward()
            self.optimizer.step()
            if batch_idx % self.args.log_interval == 0:
                print(
                    "Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}".format(
                        epoch,
                        batch_idx * len(data),
                        len(self.train_loader.dataset),
                        100.0 * batch_idx / len(self.train_loader),
                        loss.item(),
                    )
                )

    def _ecoch_test(self):
        self.model.eval()
        test_loss = 0
        correct = 0
        with torch.no_grad():
            for data, target in self.test_loader:
                data, target = data.to(self.device), target.to(self.device)
                output = self.model(data)
                test_loss += F.nll_loss(
                    output, target, reduction="sum"
                ).item()  # sum up batch loss
                pred = output.argmax(
                    dim=1, keepdim=True
                )  # get the index of the max log-probability
                correct += pred.eq(target.view_as(pred)).sum().item()

        test_loss /= len(self.test_loader.dataset)

        print(
            "\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n".format(
                test_loss,
                correct,
                len(self.test_loader.dataset),
                100.0 * correct / len(self.test_loader.dataset),
            )
        )

    def _determine_device(self):
        return torch.device("cuda" if torch.cuda.is_available() else "cpu")

    def _determine_test_loader(self, **kwargs):
        return torch.utils.data.DataLoader(
            datasets.MNIST(
                PATH_DATA,
                train=False,
                transform=transforms.Compose(
                    [
                        transforms.ToTensor(),
                        transforms.Normalize((0.1307,), (0.3081,)),
                    ]
                ),
            ),
            batch_size=self.args.test_batch_size,
            shuffle=True,
            **kwargs
        )

    def _determine_train_loader(self, **kwargs):
        return torch.utils.data.DataLoader(
            datasets.MNIST(
                PATH_DATA,
                train=True,
                download=True,
                transform=transforms.Compose(
                    [
                        transforms.ToTensor(),
                        transforms.Normalize((0.1307,), (0.3081,)),
                    ]
                ),
            ),
            batch_size=self.args.batch_size,
            shuffle=True,
            **kwargs
        )

    def _determine_model(self):
        return Net().to(self.device)

    def _determine_optimizer(self):
        return optim.Adadelta(self.model.parameters(), lr=self.args.lr)

    def _determine_scheduler(self):
        return StepLR(self.optimizer, step_size=1, gamma=self.args.gamma)

    def _set_torch_seed(self):
        torch.manual_seed(self.args.seed)
