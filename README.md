### Gitlab CI test for machine learning projects

Example taken from https://github.com/pytorch/examples/tree/master/mnist

To train model, run
```python
make model=mnist train
```